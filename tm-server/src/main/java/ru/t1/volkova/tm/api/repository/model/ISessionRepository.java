package ru.t1.volkova.tm.api.repository.model;

import ru.t1.volkova.tm.model.Session;

public interface ISessionRepository extends IAbstractUserOwnedRepository<Session> {

}
