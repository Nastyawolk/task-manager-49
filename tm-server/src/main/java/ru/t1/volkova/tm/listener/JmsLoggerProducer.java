package ru.t1.volkova.tm.listener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.yaml.YAMLMapper;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.broker.BrokerService;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.log.OperationEvent;

import javax.jms.*;
import javax.persistence.Table;
import java.lang.annotation.Annotation;

@Getter
public class JmsLoggerProducer {

    @NotNull
    private static final String URL = ActiveMQConnection.DEFAULT_BROKER_URL;

    @NotNull
    private static final String QUEUE = "LOGGER";

    @NotNull
    private final BrokerService broker = new BrokerService();

    @NotNull
    private final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(URL);

    @NotNull
    private final ObjectMapper objectMapper = new YAMLMapper();

    @NotNull
    private final ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

    @NotNull
    private final Connection connection;

    @NotNull
    private final Session session;

    @NotNull
    private final Queue destination;

    @NotNull
    private final MessageProducer messageProducer;

    @SneakyThrows
    public JmsLoggerProducer() {
        connection = connectionFactory.createConnection();
        connection.start();
        session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        destination = session.createQueue(QUEUE);
        messageProducer = session.createProducer(destination);
    }

    @SneakyThrows
    public void send(@NotNull final String text) {
        @NotNull final TextMessage message = session.createTextMessage(text);
        messageProducer.send(message);
        System.out.println(text);
    }

    public void send(@NotNull final OperationEvent event) {
        sync(event);
    }

    @SneakyThrows
    public void sync(@NotNull final OperationEvent event) {
        @NotNull final Class<?> entityClass = event.getEntity().getClass();
        if (entityClass.isAnnotationPresent(Table.class)) {
            @NotNull final Annotation annotation = entityClass.getAnnotation(Table.class);
            @NotNull final Table table = (Table) annotation;
            event.setTable(table.name());
        }
        send(objectWriter.writeValueAsString(event));
    }

}
