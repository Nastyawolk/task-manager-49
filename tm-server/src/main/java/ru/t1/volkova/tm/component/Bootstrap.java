package ru.t1.volkova.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.volkova.tm.api.endpoint.*;
import ru.t1.volkova.tm.api.repository.dto.IProjectDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.volkova.tm.api.repository.dto.ITaskDTORepository;
import ru.t1.volkova.tm.api.repository.dto.IUserDTORepository;
import ru.t1.volkova.tm.api.service.*;
import ru.t1.volkova.tm.api.service.dto.*;
import ru.t1.volkova.tm.endpoint.*;
import ru.t1.volkova.tm.enumerated.Role;
import ru.t1.volkova.tm.repository.dto.ProjectDTORepository;
import ru.t1.volkova.tm.repository.dto.SessionDTORepository;
import ru.t1.volkova.tm.repository.dto.TaskDTORepository;
import ru.t1.volkova.tm.repository.dto.UserDTORepository;
import ru.t1.volkova.tm.service.*;
import ru.t1.volkova.tm.service.dto.*;
import ru.t1.volkova.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    @Getter
    private final PropertyService propertyService = new PropertyService();

    @NotNull
    @Getter
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private final IProjectDTORepository projectRepository = new ProjectDTORepository(connectionService.getEntityManager());

    @NotNull
    private final ITaskDTORepository taskRepository = new TaskDTORepository(connectionService.getEntityManager());

    @NotNull
    private final IUserDTORepository userRepository = new UserDTORepository(connectionService.getEntityManager());

    @NotNull
    private final ISessionDTORepository sessionRepository = new SessionDTORepository(connectionService.getEntityManager());

    @NotNull
    @Getter
    private final IProjectDTOService projectService = new ProjectDTOService(projectRepository, connectionService);

    @NotNull
    @Getter
    private final ITaskDTOService taskService = new TaskDTOService(taskRepository, connectionService);

    @NotNull
    @Getter
    private final IProjectTaskDTOService projectTaskService = new ProjectTaskDTOService(projectService, taskService, connectionService);

    @NotNull
    @Getter
    private final ILoggerService loggerService = new LoggerService(connectionService);

    @NotNull
    @Getter
    private final IUserDTOService userService =
            new UserDTOService(userRepository, connectionService, propertyService, projectService, taskService);

    @NotNull
    @Getter
    private final ISessionDTOService sessionService = new SessionDTOService(sessionRepository, connectionService);

    @NotNull
    @Getter
    private final IAuthService authService = new AuthService(userService, propertyService, sessionService);

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ICalcEndpoint calcEndpoint = new CalcEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(calcEndpoint);
        registry(systemEndpoint);
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getServerHost();
        @NotNull final String port = propertyService.getServerPort().toString();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void start() {
        initPID();
        if (userService.findByLogin("admin") == null) userService.create("admin", "admin", Role.ADMIN);
        if (userService.findByLogin("user") == null) userService.create("user", "user", Role.USUAL);
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::stop));
    }

    public void stop() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
    }

}
